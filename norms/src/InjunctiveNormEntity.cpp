#include "vector"
#include "globals.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/RepastProcess.h"

#include "InjunctiveNormEntity.h"
#include "Agent.h"
#include "RegulatorInjunctiveBingePunishment.h"
#include "RegulatorInjunctiveRelaxation.h"

InjunctiveNormEntity::InjunctiveNormEntity(
		std::vector<Regulator*> regulatorList, std::vector<double> powerList, int intervalPunish, int intervalRelax,
		std::vector<std::string> normDataGate,
		std::vector<std::string> normDataGamma, std::vector<std::string> normDataLambda) :
			StructuralEntity(regulatorList, powerList, 1) { //mTransformationalInterval = 1
	mIntervalPunish = intervalPunish;
	mIntervalRelax = intervalRelax;

	//set a pointer to this entity in the relaxation regulator
	((RegulatorInjunctiveRelaxation*) mpRegulatorList[1])->setInjNormEntity(this);

	///////////
	//put data into array
	std::vector<std::string>::const_iterator injunct_iter_gate = normDataGate.begin();
	int currentRow = 0;
	int currentCol = 0;

	//create a 2D array: NUM_SEX x NUM_AGE_GROUPS
	mpInjunctiveNormsGate = new double*[NUM_SEX];
	for(int i = 0; i < NUM_SEX; ++i)
		mpInjunctiveNormsGate[i] = new double[NUM_AGE_GROUPS];

	//read values from global data
	while (injunct_iter_gate != normDataGate.end()){
		mpInjunctiveNormsGate[currentRow][currentCol] = repast::strToDouble(*injunct_iter_gate);
		currentCol++;
		if (currentCol > (NUM_AGE_GROUPS - 1)) {
			currentRow++;
			currentCol = 0;
		}
		injunct_iter_gate++;
	}
	if (!(currentRow == NUM_SEX && currentCol == 0)){
		std::cerr <<  "Missmatch in inj norm gate data read in and size intended";
	}
	///////////

	///////////
	//put data into array
	std::vector<std::string>::const_iterator injunct_iter_gamma = normDataGamma.begin();
	currentRow = 0;
	currentCol = 0;

	//create a 2D array: NUM_SEX x NUM_AGE_GROUPS
	mpInjunctiveNormsGamma = new double*[NUM_SEX];
	for(int i = 0; i < NUM_SEX; ++i)
		mpInjunctiveNormsGamma[i] = new double[NUM_AGE_GROUPS];

	//read values from global data
	while (injunct_iter_gamma != normDataGamma.end()){
		mpInjunctiveNormsGamma[currentRow][currentCol] = repast::strToDouble(*injunct_iter_gamma);
		currentCol++;
		if (currentCol > (NUM_AGE_GROUPS - 1)) {
			currentRow++;
			currentCol = 0;
		}
		injunct_iter_gamma++;
	}
	if (!(currentRow == NUM_SEX && currentCol == 0)){
		std::cerr <<  "Missmatch in inj norm gate data read in and size intended";
	}
	///////////

	///////////
	// Trying to read in the injunctive norm data for quantity.
	std::vector<std::string>::const_iterator injunct_iter_lambda = normDataLambda.begin();
	currentRow = 0;
	currentCol = 0;
	mpInjunctiveNormsLambda = new double*[NUM_SEX];

	for(int i = 0; i < NUM_SEX; ++i)
		mpInjunctiveNormsLambda[i] = new double[NUM_AGE_GROUPS];

	while (injunct_iter_lambda != normDataLambda.end()){
		mpInjunctiveNormsLambda[currentRow][currentCol] = repast::strToDouble(*injunct_iter_lambda);
		currentCol++;
		if (currentCol > (NUM_AGE_GROUPS - 1)) {
			currentRow++;
			currentCol = 0;
		}
		injunct_iter_lambda++;
	}
	if (!(currentRow == NUM_SEX && currentCol == 0)){
		std::cerr <<  "Missmatch in inj norm quant data read in and size intended";
	}
	///////////
}

InjunctiveNormEntity::~InjunctiveNormEntity() {
	for(int i = 0; i < NUM_SEX; ++i)
		delete[] mpInjunctiveNormsGamma[i];
	delete[] mpInjunctiveNormsGamma;

	for(int i = 0; i < NUM_SEX; ++i)
		delete[] mpInjunctiveNormsLambda[i];
	delete[] mpInjunctiveNormsLambda;
}

double InjunctiveNormEntity::getInjNormGate(int sex, int ageGroup) {
	return mpInjunctiveNormsGate[sex][ageGroup];
}

double InjunctiveNormEntity::getInjNormGamma(int sex, int ageGroup) {
	return mpInjunctiveNormsGamma[sex][ageGroup];
}

double InjunctiveNormEntity::getInjNormLambda(int sex, int ageGroup) {
	return mpInjunctiveNormsLambda[sex][ageGroup];
}

void InjunctiveNormEntity::doTransformation() {
	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if (currentTick % mTransformationalInterval == 0) {
		mTransformationalTriggerCount++;

		//call all regulators to update adjustment value
		RegulatorInjunctiveBingePunishment* pRegPunish = (RegulatorInjunctiveBingePunishment*) mpRegulatorList[0];
		RegulatorInjunctiveRelaxation* pRegRelax = (RegulatorInjunctiveRelaxation*) mpRegulatorList[1];

		if (currentTick % mIntervalPunish == 0)
			pRegPunish->updateAdjustmentLevel();
		else
			pRegPunish->resetAdjustmentLevel();

		if (currentTick % mIntervalRelax == 0)
			pRegRelax->updateAdjustmentLevel();
		else
			pRegRelax->resetAdjustmentLevel();

		//update injunctive norm based on adjustment level values from regulators
		for (int currentSex = 0; currentSex != NUM_SEX; ++currentSex) {
			for (int currentAgeGroup = 0; currentAgeGroup != NUM_AGE_GROUPS; ++currentAgeGroup) {
				mpInjunctiveNormsGate[currentSex][currentAgeGroup] *=
					( mpPowerList[0] * pRegPunish->getAdjustmentLevelGamma(currentSex, currentAgeGroup) +
					mpPowerList[1] * pRegRelax->getAdjustmentLevelGamma(currentSex, currentAgeGroup) );
				if (mpInjunctiveNormsGate[currentSex][currentAgeGroup] > 1) {
					mpInjunctiveNormsGate[currentSex][currentAgeGroup] = 1;
				}

				mpInjunctiveNormsGamma[currentSex][currentAgeGroup] *=
					( mpPowerList[0] * pRegPunish->getAdjustmentLevelGamma(currentSex, currentAgeGroup) +
					mpPowerList[1] * pRegRelax->getAdjustmentLevelGamma(currentSex, currentAgeGroup) );
				if (mpInjunctiveNormsGamma[currentSex][currentAgeGroup] > 1) {
					mpInjunctiveNormsGamma[currentSex][currentAgeGroup] = 1;
				}

				mpInjunctiveNormsLambda[currentSex][currentAgeGroup] *=
					( mpPowerList[0] * pRegPunish->getAdjustmentLevelLambda(currentSex, currentAgeGroup) +
					mpPowerList[1] * pRegRelax->getAdjustmentLevelLambda(currentSex, currentAgeGroup) );
			}
		}

		// EXPERIMENT 3
		if (EXPERIMENT==3) {
			if(repast::RepastProcess::instance()->getScheduleRunner().currentTick() >= 365*5 && !mInterventionFlag){
				//std::cout << "==B== " << repast::RepastProcess::instance()->getScheduleRunner().currentTick() << std::endl;
				for (int currentSex = 0; currentSex != NUM_SEX; ++currentSex) {
					for (int currentAgeGroup = 0; currentAgeGroup != NUM_AGE_GROUPS; ++currentAgeGroup) {
						//std::cout << currentSex <<"\t" << currentAgeGroup <<"\t" <<mpInjunctiveNormsGate[currentSex][currentAgeGroup];
						mpInjunctiveNormsGate[currentSex][currentAgeGroup] /= 2;
						//std::cout << "\t" << mpInjunctiveNormsGate[currentSex][currentAgeGroup] << std::endl;
					}
				}
				//std::cout << "==E== " << repast::RepastProcess::instance()->getScheduleRunner().currentTick() << std::endl << std::endl;
				mInterventionFlag = true;
			}
		}

		/*std::cout << "==GAMMA== ";
		for (int currentSex = 0; currentSex != NUM_SEX; ++currentSex) {
			for (int currentAgeGroup = 0; currentAgeGroup != NUM_AGE_GROUPS; ++currentAgeGroup) {
				std::cout << mpInjunctiveNormsGamma[currentSex][currentAgeGroup] << " ";
			}
		}
		std::cout << std::endl;

		std::cout << "==LAMBDA== ";
		for (int currentSex = 0; currentSex != NUM_SEX; ++currentSex) {
			for (int currentAgeGroup = 0; currentAgeGroup != NUM_AGE_GROUPS; ++currentAgeGroup) {
				std::cout << mpInjunctiveNormsLambda[currentSex][currentAgeGroup] << " ";
			}
		}
		std::cout << std::endl;*/
	}
}
