#include "Theory.h"
#include <math.h>
#include <boost/unordered_map.hpp>
#include "globals.h"
#include <iostream>
#include <boost/mpi.hpp>

void Theory::setAgent(Agent *agent) {
	mpAgent = agent;
}

std::pair<double, double> Theory::generateCorrectedMeanSD (double desiredMean, double desiredSd){
	int hashKey;
	std::pair<double, double> localPair = std::make_pair(0.0, 0.0);

	if (desiredMean < 0){
		std::cerr << "drink mean must be >= 0. " << desiredMean << " , " << desiredSd << std::endl;
		//throw MPI::Exception(MPI::ERR_INTERN);
		desiredMean = 0;
	}
	if (desiredSd < 0){
		std::cerr << "drink sd must be >= 0. " << desiredMean << " , " << desiredSd << std::endl;
		//throw MPI::Exception(MPI::ERR_INTERN);
		desiredSd = 0;
	}

	//quick fix when sd too small ~0
	if (desiredSd < .05){
		return (std::make_pair(desiredMean, 0)) ;
	}

	//quick fix for lookup table: desireMean not exist for [0..1]
	if (desiredMean < 1) {
		return (std::make_pair(desiredMean, desiredSd)) ;
	}

	//quick fix for lookup table: mean in range but sd out of range
	if (round(desiredMean) < 10 && desiredMean >=1 && desiredSd>3.15) {
		return (std::make_pair(desiredMean, desiredSd)) ;
	}

	if ((round(desiredMean) < 10 && desiredMean >=1 && desiredSd<=3.15 && desiredSd > 0.05) || (desiredMean > 10 && desiredSd <= 0.7 && desiredSd > 0.05)){
		localPair = doLookup(desiredMean, desiredSd);
		return localPair;
	}else{
		localPair = doFunctionLookup(desiredMean, desiredSd);
		return localPair;
	}
}

std::pair<double, double> Theory::doLookup(double mean, double sd){
	int roundedMean = (int)round(mean *10); //x10 then rounded. eg 2.1 => 21
        int roundedSD = (int)((round(sd*100) + 5/2) / 5) * 5; //x100 then round to the nearest 5. eg 0.11 => 10 or 0.14 => $
        //if (roundedSD == 0) roundedSD = 5;
        int hashKey = roundedMean * 1000 + roundedSD;
	try {
		return MEAN_SD_LOOKUP_TABLE.at(hashKey);
	} catch(const std::out_of_range& oor) {
		std::cerr << mean <<" "<< sd << " => " << roundedMean <<" "<< roundedSD <<
			" => "<< hashKey << " hashkey not in MEAN_SD_LOOKUP_TABLE map. " << std::endl;
		throw MPI::Exception(MPI::ERR_INTERN);
	}
}

std::pair<double, double> Theory::doFunctionLookup(double desiredMean, double desiredSd){
	double intercept = .9931*desiredMean - 0.7776;
	double quadCoeff = .3613 + 0.66*exp(-0.66*(desiredMean - 6.54));
	double linearCoeff = .3957-exp(-7.43*(desiredMean - 0.623));
	double newMean = quadCoeff*desiredSd*desiredSd + linearCoeff*desiredSd + intercept;
	double newSD = 1.7245*desiredSd - 0.592;
	return std::make_pair(newMean, newSD);
}
