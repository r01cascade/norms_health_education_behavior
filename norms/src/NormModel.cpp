#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include <vector>
#include <stdio.h>

#include "NormModel.h"

#include "Agent.h"
#include "NormStatisticsCollector.h"
#include "TheoryMediator.h"
#include "Theory.h"
#include "StructuralEntity.h"
#include "MediatorForOneTheory.h"
#include "NormTheory.h"
#include "InjunctiveNormEntity.h"
#include "RegulatorInjunctiveBingePunishment.h"
#include "RegulatorInjunctiveRelaxation.h"
#include "DescriptiveNormEntity.h"

NormModel::NormModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm) :
					Model(propsFile, argc, argv, comm) {
	// Read parameters for injunctive norms and payoff adjustment from model.props file
	std::string strInjThreshold = props->getProperty("norms.injunctive.threshold");
	std::string strInjProportion = props->getProperty("norms.injunctive.proportion");
	std::string strInjRelaxationGammaAdjustment = props->getProperty("norms.injunctive.relaxation.gamma.adjustment");
	std::string strInjRelaxationLambdaAdjustment = props->getProperty("norms.injunctive.relaxation.lambda.adjustment");
	std::string strInjPunishmentGammaAdjustment = props->getProperty("norms.injunctive.punishment.gamma.adjustment");
	std::string strInjPunishmentLambdaAdjustment = props->getProperty("norms.injunctive.punishment.lambda.adjustment");
	std::string strNDaysDes = props->getProperty("norms.n.days.descriptive");
	std::string strCompDaysPunish = props->getProperty("norms.com.days.punish");
	std::string strCompDaysRelax = props->getProperty("norms.com.days.relax");
	std::string strPerceptionBias = props->getProperty("bias.factor");
	std::string strDiscountMale = props->getProperty("discount.male");
	std::string strDiscountFemale = props->getProperty("discount.female");
	std::string strDesireMultipilerAbstainer = props->getProperty("desire.multiplier.abstainer");
	std::string strDesireMultipilerDrinker = props->getProperty("desire.multiplier.drinker");

	if (!strInjThreshold.empty()) { INJUNCTIVE_THRESHOLD = repast::strToInt(strInjThreshold); }
	if (!strInjProportion.empty()) { INJUNCTIVE_PROPORTION = repast::strToDouble(strInjProportion); }
	if (!strInjRelaxationGammaAdjustment.empty()) { INJ_RELAXATION_GAMMA_ADJUSTMENT = repast::strToDouble(strInjRelaxationGammaAdjustment); }
	if (!strInjRelaxationLambdaAdjustment.empty()) { INJ_RELAXATION_LAMBDA_ADJUSTMENT = repast::strToDouble(strInjRelaxationLambdaAdjustment); }
	if (!strInjPunishmentGammaAdjustment.empty()) { INJ_PUNISHMENT_GAMMA_ADJUSTMENT = repast::strToDouble(strInjPunishmentGammaAdjustment); }
	if (!strInjPunishmentLambdaAdjustment.empty()) { INJ_PUNISHMENT_LAMBDA_ADJUSTMENT = repast::strToDouble(strInjPunishmentLambdaAdjustment); }
	if (!strNDaysDes.empty()) {N_DAYS_DESCRIPTIVE = repast::strToDouble(strNDaysDes); }
	if (!strCompDaysPunish.empty()) {COMP_DAYS_PUNISH = repast::strToDouble(strCompDaysPunish); }
	if (!strCompDaysRelax.empty()) {COMP_DAYS_RELAX = repast::strToDouble(strCompDaysRelax); }
	if (!strPerceptionBias.empty()) {PERCEPTION_BIAS = repast::strToDouble(strPerceptionBias);}
	if (!strDiscountMale.empty()) {DISCOUNT_MALE = repast::strToDouble(strDiscountMale);}
	if (!strDiscountFemale.empty()) {DISCOUNT_FEMALE = repast::strToDouble(strDiscountFemale);}
	if (!strDesireMultipilerAbstainer.empty()) {DESIRE_MULTIPLIER_ABSTAINER = repast::strToDouble(strDesireMultipilerAbstainer);}
	if (!strDesireMultipilerDrinker.empty()) {DESIRE_MULTIPLIER_DRINKER = repast::strToDouble(strDesireMultipilerDrinker);}

	//read parameters from file and store in a table for init agents function (used by Model as well)
	int rank = repast::RepastProcess::instance()->rank();
	std::string rankFileNameProperty = "file.rank" + std::to_string(rank);
	std::string rankFileName = props->getProperty(rankFileNameProperty);
	readRankFileForTheory(rankFileName);

	//regulators that affect the structural entities
	std::vector<Regulator*> regulatorListInj;
	mpRegPunishment = new RegulatorInjunctiveBingePunishment(&context);
	mpRegRelaxation = new RegulatorInjunctiveRelaxation(&context);
	regulatorListInj.push_back(mpRegPunishment);
	regulatorListInj.push_back(mpRegRelaxation);

	//power of each regulator (sum = 1)
	std::vector<double> powerListInj;
	powerListInj.push_back(0.5);
	powerListInj.push_back(0.5);

	//dummy list for Des Norm
	std::vector<Regulator*> regulatorListDes;
	std::vector<double> powerListDes;

	//create a structural entities
	std::vector<std::string> injNormGate;
	repast::tokenize(props->getProperty("norms.injunctive.gate"), injNormGate, ",");
	std::vector<std::string> injNormGamma;
	repast::tokenize(props->getProperty("norms.injunctive.gamma"), injNormGamma, ",");
	std::vector<std::string> injNormLambda;
	repast::tokenize(props->getProperty("norms.injunctive.lambda"), injNormLambda, ",");

	mIntervalDesNorm = repast::strToInt(props->getProperty("transformational.interval.descriptive.norm"));
	mIntervalPunish = repast::strToInt(props->getProperty("transformational.interval.punish"));
	mIntervalRelax = repast::strToInt(props->getProperty("transformational.interval.relax"));

	InjunctiveNormEntity *pInjNorm = new InjunctiveNormEntity(regulatorListInj, powerListInj, mIntervalPunish, mIntervalRelax, injNormGate, injNormGamma, injNormLambda);
	DescriptiveNormEntity *pDesNorm = new DescriptiveNormEntity(regulatorListDes, powerListDes, mIntervalDesNorm, &context);
	structuralEntityList.push_back(pInjNorm);
	structuralEntityList.push_back(pDesNorm);

	//Norm outputs (1 core)
	if (AGENT_LEVEL_OUTPUT) {
		std::string annualFileName = addUniqueSuffix("outputs/annual_norm_output.csv");
		annualNormOutput.open(annualFileName);
		annualNormOutput << "Year,";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "DesPrevelance" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "DesQuant" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "InjGate" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "InjGamma" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "InjLambda" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "IntervalPunishment" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "IntervalRelaxation" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "PopulationCount" << i << j << ",";
		annualNormOutput << std::endl;
	}

	EXPERIMENT = props->getProperty("experiment").empty() ? 0 : repast::strToInt(props->getProperty("experiment"));
}

NormModel::~NormModel() {
	annualNormOutput.close();
}

//read parameters from file and store in a table
void NormModel::readRankFileForTheory(std::string rankFileName) {
#ifdef DEBUG
		std::cout << "Reading the file: " << rankFileName <<std::endl;
#endif
	ifstream myfile(rankFileName);
	if (myfile.is_open()) {
		//read the csv file
		infoTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << rankFileName << std::endl;
	}

	//find index of theory-specific variables
	mIndexAutonomy = -1;
	std::vector<string> headerLine = infoTable.front();
	for (int i = 0; i < headerLine.size(); ++i) {
		if ( headerLine[i]=="norms.autonomy" )
			mIndexAutonomy = i;
	}
	if (mIndexAutonomy==-1)
		std::cerr << "Index (theory-specific) not found" << std::endl;
}

void NormModel::initMediatorAndTheoryWithRandomParameters(Agent *agent) {
	//create theory(ies) and a mediator for each agent
	std::vector<Theory*> theoryList;
	NormTheory* theory = new NormTheory(&context, (InjunctiveNormEntity *) structuralEntityList[0],
				(DescriptiveNormEntity *) structuralEntityList[1]);
	theoryList.push_back(theory);
	TheoryMediator *mediator = new MediatorForOneTheory(theoryList);

	//link agent with the mediator
	agent->setMediator(mediator);
	theory->initDesires();
}

void NormModel::initMediatorAndTheoryFromFile(Agent *agent, std::vector<std::string> info) {
	double autonomy = repast::strToDouble(info[mIndexAutonomy]);

	//create theory(ies) and a mediator for each agent
	std::vector<Theory*> theoryList;
	NormTheory* theory = new NormTheory(&context, (InjunctiveNormEntity *) structuralEntityList[0],
				(DescriptiveNormEntity *) structuralEntityList[1], autonomy);
	theoryList.push_back(theory);
	TheoryMediator *mediator = new MediatorForOneTheory(theoryList);

	//link agent with the mediator
	agent->setMediator(mediator);
	theory->initDesires();
}

void NormModel::initForTheory(repast::ScheduleRunner& runner) {
	//update avg drinking values before 1st situational mechanism
	((DescriptiveNormEntity *) structuralEntityList[1])->updateDescriptiveGroupDrinking();
}

std::string NormModel::addUniqueSuffix(std::string fileName){

	if (!boost::filesystem::exists(fileName)){ //check if the file doesn't exist.
		return fileName;				       //if not, return the filename as is.

	}else{
		//I will assume that if the file basename doesn't exists, none of the numbered versions do either.
		//If numbered versions exist while the base fileName file doens't exist, this will overwrite them.
		int i = 1;
		std::string noExtensionFileName = fileName;
		for (int count = 0; count < 4; ++count){
			noExtensionFileName.pop_back();
		}
		std::string testFileName = noExtensionFileName + "_" + to_string(i) + ".csv";
		while (boost::filesystem::exists(testFileName))
		{
			++i;
			testFileName = noExtensionFileName + "_" + to_string(i) + ".csv";
		}
		return testFileName;
	}
}

void NormModel::writeAnnualAgentDataToFile() {
	if (AGENT_LEVEL_OUTPUT && annualNormOutput.is_open()){
		annualNormOutput << simYear << ",";
		DescriptiveNormEntity* pDesNormEntity = (DescriptiveNormEntity *) structuralEntityList[1];
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << pDesNormEntity->getAvgIsDrinking(i,j) << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << pDesNormEntity->getAvgNumberDrinks(i,j) << ",";

		InjunctiveNormEntity* pInjNormEntity = (InjunctiveNormEntity *) structuralEntityList[0];
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << pInjNormEntity->getInjNormGate(i,j) << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << pInjNormEntity->getInjNormGamma(i,j) << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << pInjNormEntity->getInjNormLambda(i,j) << ",";

		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << mpRegPunishment->mTransformationalTriggerCount[i][j]/(365.0/mIntervalPunish) << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << mpRegRelaxation->mTransformationalTriggerCount[i][j]/(365.0/mIntervalRelax) << ",";

		for (int i=0; i<NUM_SEX; ++i) {
			for (int j=0; j<NUM_AGE_GROUPS; ++j) {
				int sum = 0;
				repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
				repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
				while (iter != iterEnd) {
					if ((*iter)->getSex() == i && (*iter)->findAgeGroup() == j) {
						sum += 1;
					}
					iter++;
				}
				annualNormOutput << sum << ",";
			}
		}

		annualNormOutput << std::endl;

		pInjNormEntity->mTransformationalTriggerCount = 0;
		pDesNormEntity->mTransformationalTriggerCount = 0;
		mpRegPunishment->resetCount();
		mpRegRelaxation->resetCount();
	} else {
		std::cerr << "Error: Can't write to annual_norm_output.csv, file not open." << std::endl;
	}
}


void NormModel::initStatisticCollector() {
	mpCollector = new NormStatisticsCollector(&context);
}

void NormModel::addTheoryAnnualData(repast::SVDataSetBuilder& builder) {
	// Drinking from the targeted group of Exp 2
	StatDataSource<int>* outPop_G3G4_M = mpCollector->getDataSource<int>("Pop_G3G4_M");
	builder.addDataSource(repast::createSVDataSource("Pop_G3G4_M", outPop_G3G4_M, std::plus<int>()));
	StatDataSource<int>* outPop_G3G4_F = mpCollector->getDataSource<int>("Pop_G3G4_F");
	builder.addDataSource(repast::createSVDataSource("Pop_G3G4_F", outPop_G3G4_F, std::plus<int>()));

	StatDataSource<int>* outDrinker_G3G4_M = mpCollector->getDataSource<int>("Drinker_G3G4_M");
	builder.addDataSource(repast::createSVDataSource("Drinker_G3G4_M", outDrinker_G3G4_M, std::plus<int>()));
	StatDataSource<int>* outDrinker_G3G4_F = mpCollector->getDataSource<int>("Drinker_G3G4_F");
	builder.addDataSource(repast::createSVDataSource("Drinker_G3G4_F", outDrinker_G3G4_F, std::plus<int>()));

	StatDataSource<double>* outQuant_G3G4_M = mpCollector->getDataSource<double>("Quant_G3G4_M");
	builder.addDataSource(repast::createSVDataSource("Quant_G3G4_M", outQuant_G3G4_M, std::plus<double>()));
	StatDataSource<double>* outQuant_G3G4_F = mpCollector->getDataSource<double>("Quant_G3G4_F");
	builder.addDataSource(repast::createSVDataSource("Quant_G3G4_F", outQuant_G3G4_F, std::plus<double>()));

	StatDataSource<int>* outFreq_G3G4_M = mpCollector->getDataSource<int>("Freq_G3G4_M");
	builder.addDataSource(repast::createSVDataSource("Freq_G3G4_M", outFreq_G3G4_M, std::plus<int>()));
	StatDataSource<int>* outFreq_G3G4_F = mpCollector->getDataSource<int>("Freq_G3G4_F");
	builder.addDataSource(repast::createSVDataSource("Freq_G3G4_F", outFreq_G3G4_F, std::plus<int>()));
}