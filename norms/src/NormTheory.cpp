#include "Agent.h"
#include "Theory.h"
#include "NormTheory.h"
#include <math.h>
#include "repast_hpc/RepastProcess.h"

NormTheory::NormTheory(repast::SharedContext<Agent> *pPopulation, InjunctiveNormEntity *pInjNormEntity,
		DescriptiveNormEntity *pDesNormEntity) {
	mAutonomy = double (repast::Random::instance()->nextDouble());

	mpPopulation = pPopulation;
	mpInjNormEntity = pInjNormEntity;
	mpDesNormEntity = pDesNormEntity;
}

NormTheory::NormTheory(repast::SharedContext<Agent> *pPopulation, InjunctiveNormEntity *pInjNormEntity,
		DescriptiveNormEntity *pDesNormEntity, double autonomy) {
	mAutonomy = autonomy;

	mpPopulation = pPopulation;
	mpInjNormEntity = pInjNormEntity;
	mpDesNormEntity = pDesNormEntity;
}

void NormTheory::initDesires() {
	//prob of desire gateway: frequency => random num of days => bump prob down for freq 1
	int freq = mpAgent->getDrinkFrequencyLevel();
	int freqLowerBound[7] = {0 ,11,19,31, 54,172,337};
	int freqUpperBound[7] = {10,18,30,53,171,336,365};
	int dayLowerBound = freqLowerBound[freq-1];
	int dayUpperBound = freqUpperBound[freq-1];
	repast::IntUniformGenerator rnd = repast::Random::instance()->createUniIntGenerator(dayLowerBound, dayUpperBound);
	int randomDrinkingDaysPerYear = rnd.next();
	mDesireGateway = (double)randomDrinkingDaysPerYear/365.0;
	if (freq<3) { //bump down for freq < 3
		mDesireGateway *= DESIRE_MULTIPLIER_ABSTAINER;
	} 
	if(freq > 6) { //bump up for freq > 4
		mDesireGateway *= DESIRE_MULTIPLIER_DRINKER;
	}

	//desire mean and sd: init (once) from drinking history
	mDesireMean = mpAgent->getPastYearMeanDrink();
	//std::cout << "This is the desired mean: " << mDesireMean << std::endl;
	if (mDesireMean>1) { //bump up for drinkers for freq >2
		mDesireMean *= DESIRE_MULTIPLIER_DRINKER;
		}
	mDesireSd = mpAgent->getPastYearSdDrink();
}

NormTheory::~NormTheory() {
	agentTrackingOutput.close();
}

void NormTheory::doSituation() {
	calcDescriptiveNorm();

	//AGENT-TRACKING FEATURE
	/*if (!agentTrackingOutput.is_open()) {
		int agentID = mpAgent->getId().id();
		if ((agentID==102 || agentID==549)) {
			std::string agentTrackingFileName("outputs/agent_" + to_string(agentID) + "_output.csv");
			agentTrackingOutput.open(agentTrackingFileName);
			agentTrackingOutput << "AgentId, Tick, mAutonomy, mDesireGateway, InjNormGate, mDescriptiveNormGate, mDesireMean, InjNormGamma, InjNormLambda, mDescriptiveNormQuant, mDescriptiveNormQuantSd, Freq, Quant, HED" << std::endl;
		}
	}
	if ((mpAgent->getId().id()==102 || mpAgent->getId().id()==549) && agentTrackingOutput.is_open()){
		int mySex = int(mpAgent->getSex());
		int myAgeGroup = mpAgent->findAgeGroup();
		agentTrackingOutput << mpAgent->getId().id() << "," << 
			repast::RepastProcess::instance()->getScheduleRunner().currentTick() << "," << mAutonomy << "," << 
			mDesireGateway << "," << mpInjNormEntity->getInjNormGate(mySex,myAgeGroup) << "," << mDescriptiveNormGate << "," << 
			mDesireMean << "," << mpInjNormEntity->getInjNormGamma(mySex,myAgeGroup) << "," << 
			mpInjNormEntity->getInjNormLambda(mySex,myAgeGroup) << "," << 
			mDescriptiveNormQuant << "," << mDescriptiveNormQuantSd << "," <<
			mpAgent->getNumDaysHavingKDrinksOverNDays(30,1) << "," <<
			mpAgent->getAvgDrinksNDays(30) << "," <<
			mpAgent->getNumDaysHavingKDrinksOverNDays(30,5) << std::endl;
	}*/
}

double NormTheory::calcDispositionFunc(double autonomy, double payoff, double injunctive, double descriptive) {
	return (payoff * autonomy) + ( (sqrt(injunctive*descriptive)) * (1 - autonomy));
}

void NormTheory::doGatewayDisposition() {
    /*Action mechanism determining drinking behavior*/
    // calculate individual drinking value
	mOneTimeRandomForDisposition = repast::Random::instance()->nextDouble(); //use for both gateway and next-drink disposition
	int mySex = int(mpAgent->getSex());
	int myAgeGroup = mpAgent->findAgeGroup();
	// TEMP: EXPERIMENT 2A increase desire of a certain agent after a certain time
	// EXAMPLE: after 5 years, male, age group 3 & 4 (35-44, 45-54 years, 0 is the youngest) => desire*1.25
	if (EXPERIMENT==2) {
		if (repast::RepastProcess::instance()->getScheduleRunner().currentTick() == 365*10 &&
				mySex == MALE && (myAgeGroup == 3 |myAgeGroup == 4) ) {
				mDesireGateway *= 1.25;
		}
	}
	double drinkingValue = calcDispositionFunc(mAutonomy, mDesireGateway,
			mpInjNormEntity->getInjNormGate(mySex,myAgeGroup), mDescriptiveNormGate);
		if(drinkingValue > mOneTimeRandomForDisposition) {
		mpAgent->setDispositionByIndex(0, 1); //set first disposition to drinking value
	} else {
		mpAgent->setDispositionByIndex(0, 0); //set first disposition to drinking value
	}

//	mpAgent->setDispositionByIndex(0, drinkingValue); //set first disposition to drinking value
}

void NormTheory::doNextDrinksDisposition() {
	// Define mean and SD of the normal distribution to calculate probability of the suggested number of drinks
	int mySex = int(mpAgent->getSex());
	int myAgeGroup = mpAgent->findAgeGroup();
	double meanDescriptive = mDescriptiveNormQuant;
	double sdDescriptive = mDescriptiveNormQuantSd;
	double injunctive = mpInjNormEntity->getInjNormLambda(mySex,myAgeGroup);

	// TEMP: EXPERIMENT 2B increase desire of a certain agent after a certain time
	// EXAMPLE: after 5 years, male, age group 3 & 4 (35-44, 45-54 years, 0 is the youngest) => desire*1.25
	if (EXPERIMENT==2) {
		if (repast::RepastProcess::instance()->getScheduleRunner().currentTick() == 365*10 &&
				mySex == MALE && (myAgeGroup == 3 |myAgeGroup == 4) ) {
			mDesireMean *= 1.25;
		}
	}
	
	// fill in the vector of drinking dispositions
	for (int i=1; i<MAX_DRINKS; ++i) {
		if (mDesireMean>MAX_DRINKS)
			mDesireMean = MAX_DRINKS;
		std::pair<double, double> payoffPair = generateCorrectedMeanSD(mDesireMean, mDesireSd);
		double payoffMean = payoffPair.first;
		double payoffSD = payoffPair.second;
		double payoff = 1 - (0.5 * erfc(-(i-payoffMean)/(payoffSD*sqrt(2))));

		double descriptive;
		if (sdDescriptive == 0) {
			//std::cout << "There is no variation in descriptive norm." << std::endl;
			descriptive = meanDescriptive;
		} else {
			if (meanDescriptive>MAX_DRINKS)
				meanDescriptive = MAX_DRINKS;
			std::pair<double, double> drinkPair = generateCorrectedMeanSD(meanDescriptive, sdDescriptive);
			double correctedMean = drinkPair.first;
			double correctedSD = drinkPair.second;
			descriptive = 1 - (0.5 * erfc(-(i-correctedMean)/(correctedSD*sqrt(2))));
		}

		if (descriptive <= 0 ) {
			descriptive = 0;
		}
		if (injunctive > 0 ) {
			injunctive = mpInjNormEntity->getInjNormGamma(mySex,myAgeGroup) *
					exp(-mpInjNormEntity->getInjNormLambda(mySex,myAgeGroup)*(i-1));
		} else {
			injunctive = 0;
		}

		int tempDisposition;
		double drinkingValue = calcDispositionFunc(mAutonomy, payoff, injunctive, descriptive);
		if ( drinkingValue > repast::Random::instance()->nextDouble() )
			tempDisposition = 1;
		else tempDisposition = 0;
		mpAgent->setDispositionByIndex(i, tempDisposition);
	}

}

void NormTheory::doNonDrinkingActions() {
	//do nothing
}

void NormTheory::calcDescriptiveNorm() {
	double weights = 0;
	double weightedDrinking = 0;
	double weightedDrinkingQuant = 0;
	double weightedDrinkingSd = 0;
	double pastNDaysMeanDrink = mpAgent->getAvgDrinksNDays(N_DAYS_DESCRIPTIVE, true);
	for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j) {
				weights += compareToMe(i,j);
				weightedDrinking += compareToMe(i,j) * mpDesNormEntity->getAvgIsDrinking(i,j);
				weightedDrinkingQuant += compareToMe(i,j) * mpDesNormEntity->getAvgNumberDrinks(i,j);
				//std::cout << "descriptive on quantity per group " << mpDesNormEntity->getAvgNumberDrinks(i,j)  << std::endl;
				weightedDrinkingSd += compareToMe(i,j) * mpDesNormEntity->getSdNumberDrinks(i, j);
			}

	if (weights != 0) {
		mDescriptiveNormGate = weightedDrinking/weights;
		mDescriptiveNormQuant = weightedDrinkingQuant/weights;
		// Introduce bias in perceiving the norm
		//std::cout << "descriptive before bias " << mDescriptiveNormQuant  << std::endl;
		// EXPERIMENT 1 
		if (EXPERIMENT==0) {
			mDescriptiveNormQuant = mDescriptiveNormQuant * PERCEPTION_BIAS +  pastNDaysMeanDrink * (1-PERCEPTION_BIAS);
		} else if (EXPERIMENT==1) {
			if(repast::RepastProcess::instance()->getScheduleRunner().currentTick() >= 365*5 && pastNDaysMeanDrink>mDescriptiveNormQuant){
				mDescriptiveNormQuant = mDescriptiveNormQuant * 0.95 +  pastNDaysMeanDrink * 0.05;
			} else {
				mDescriptiveNormQuant = mDescriptiveNormQuant * PERCEPTION_BIAS +  pastNDaysMeanDrink * (1-PERCEPTION_BIAS);
			}
		}
		//std::cout << "descriptive after bias " << mDescriptiveNormQuant << std::endl;
		mDescriptiveNormQuantSd = weightedDrinkingSd/weights;
		
	} else {
		std::cerr << "Descriptive Norm: weights should NOT be 0." << std::endl;
//		mDescriptiveNormGate = DEFAULT_D_NORM;
//		mDescriptiveNormQuant = DEFAULT_D_NORM_QUANT;
//		mDescriptiveNormQuantSd = DEFAULT_D_NORM_QUANT_SD;
	}
}

int NormTheory::compareToMe(int sex, int ageGroup) {
	// declare local integer as counter
	int numberShared = 0;

	// compare and count number of shared attributes
	numberShared += int (mpAgent->getSex() == sex);
	numberShared += int (mpAgent->findAgeGroup() == ageGroup);

	// return number of shared attributes
	return numberShared;
}
