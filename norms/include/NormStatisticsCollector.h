#ifndef INCLUDE_NORM_STATCOLLECTOR_H_
#define INCLUDE_NORM_STATCOLLECTOR_H_

#include "StatisticsCollector.h"

class NormStatisticsCollector : public StatisticsCollector {

public:
	NormStatisticsCollector(repast::SharedContext<Agent>* pPopulation);
	void collectTheoryAgentStatistics() override;
};

#endif /* INCLUDE_NORM_STATCOLLECTOR_H_ */