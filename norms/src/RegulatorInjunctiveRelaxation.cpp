#include "RegulatorInjunctiveRelaxation.h"

RegulatorInjunctiveRelaxation::RegulatorInjunctiveRelaxation(repast::SharedContext<Agent> *context) {
	mpContext = context;

	//create a 2D array: noRow x noCol
	adjustmentLevelsGamma = new double*[NUM_SEX];
	adjustmentLevelsLambda = new double*[NUM_SEX];
	mTransformationalTriggerCount = new int*[NUM_SEX];
	for(int i = 0; i < NUM_SEX; ++i) {
		adjustmentLevelsGamma[i] = new double[NUM_AGE_GROUPS];
		adjustmentLevelsLambda[i] = new double[NUM_AGE_GROUPS];
		mTransformationalTriggerCount[i] = new int[NUM_AGE_GROUPS];
	}

	resetCount();
}

RegulatorInjunctiveRelaxation::~RegulatorInjunctiveRelaxation() {
	for(int i = 0; i < NUM_SEX; ++i) {
		delete[] adjustmentLevelsGamma[i];
		delete[] adjustmentLevelsLambda[i];
		delete[] mTransformationalTriggerCount[i];
	}
	delete[] adjustmentLevelsGamma;
	delete[] adjustmentLevelsLambda;
	delete[] mTransformationalTriggerCount;
}

void RegulatorInjunctiveRelaxation::resetCount() {
	for (int i=0; i<NUM_SEX; ++i)
		for (int j=0; j<NUM_AGE_GROUPS; ++j)
			mTransformationalTriggerCount[i][j] = 0;
}

void RegulatorInjunctiveRelaxation::resetAdjustmentLevel() {
	for (int i=0; i<NUM_SEX; ++i)
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			adjustmentLevelsGamma[i][j] = 1;
			adjustmentLevelsLambda[i][j] = 1;
		}
}

void RegulatorInjunctiveRelaxation::updateAdjustmentLevel() {
	std::vector<Agent*> agents;
	mpContext->selectAgents(mpContext->size(), agents);

	int countN [NUM_SEX][NUM_AGE_GROUPS];
	int countCurrentDrinker [NUM_SEX][NUM_AGE_GROUPS];
	for (int i=0; i<NUM_SEX; ++i)
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			countN[i][j] = 0;
			countCurrentDrinker[i][j] = 0;
		}

	//loop through agent and count num of heavy drinkers
	std::vector<Agent*>::const_iterator iter = agents.begin();
	std::vector<Agent*>::const_iterator iterEnd = agents.end();
	while (iter != iterEnd) {
		++countN[(*iter)->getSex()][(*iter)->findAgeGroup()];
		if ((*iter)->isHaveKDrinksOverNDays(COMP_DAYS_RELAX, 1)) {
			++countCurrentDrinker[(*iter)->getSex()][(*iter)->findAgeGroup()];
		}
		iter++;
	}

	for (int i=0; i<NUM_SEX; ++i)
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			double proportionCurrentDrinkers = 0;
			if (countN[i][j] != 0){
				proportionCurrentDrinkers = double(countCurrentDrinker[i][j]) / double(countN[i][j]);
			} else {
				//std::cout << "There are no people in this demographic here" << std::endl;
			}

			//std::cout << "== descriptive proportion drinkers: " << i << " " << j << " " << proportionCurrentDrinkers << std::endl;
			if (proportionCurrentDrinkers > mpInjunctiveNormEntity->getInjNormGamma(i,j)) {
				mTransformationalTriggerCount[i][j]++;
				adjustmentLevelsGamma[i][j] = INJ_RELAXATION_GAMMA_ADJUSTMENT;
				adjustmentLevelsLambda[i][j] = INJ_RELAXATION_LAMBDA_ADJUSTMENT;
				//std::cout << "The injunctive norm was adjusted (relax): " << i << " " << j << std::endl;
			} else {
				adjustmentLevelsGamma[i][j] = 1;
				adjustmentLevelsLambda[i][j] = 1;
				//std::cout << "The injunctive norm was unchanged: " << i << " " << j << " " << mpInjunctiveNormEntity->getInjNormGate(i,j) << std::endl;
			}
		}
/* #ifdef DEBUG
				std::stringstream msg;
				msg
						<< "Heavy drinkers had to be punished. The new injunctive norm of this group is now: "
						<< mpInjunctiveNorms[currentSex][currentAgeGroup]
						<< "\n";
				std::cout << msg.str();
#endif */


}

void RegulatorInjunctiveRelaxation::setInjNormEntity(InjunctiveNormEntity *pInjNormEntity) {
	mpInjunctiveNormEntity = pInjNormEntity;
}

double RegulatorInjunctiveRelaxation::getAdjustmentLevelGamma(int sex, int ageGroup) {
	return adjustmentLevelsGamma[sex][ageGroup];
}

double RegulatorInjunctiveRelaxation::getAdjustmentLevelLambda(int sex, int ageGroup) {
	return adjustmentLevelsLambda[sex][ageGroup];
}
