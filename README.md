# About
This is the source code for the norms model by [CASCADE project](https://www.sheffield.ac.uk/cascade) for a publication on [Health Education & Behavior journal](https://journals.sagepub.com/home/heb).

Publication title: The Normative Underpinnings of Population-Level Alcohol Use: An Individual-Level Simulation Model
Authours: Charlotte Probst; Tuong Vu; Joshua Epstein; Alexandra Nielsen; Charlotte Buckley; Alan Brennan; Jürgen Rehm; Robin Purshouse
[Health Education & Behavior link](http://doi.org/10.1177/1090198119880545)
[Free PMC article link](https://pubmed.ncbi.nlm.nih.gov/32090651/)

This repo is hereby licensed for use under the GNU GPL version 3.


# Install the RepastHPC
* Please install [RepastHPC](https://repast.github.io/repast_hpc.html). We have 2 important notes that might help with you installation.
* Installation Note 1: For our system (Ubuntu 16), before install netcdf with "./install.sh netcdf", you need to install zlib1g-dev with this command "sudo apt-get install zlib1g-dev"
* Installation Note 2: Before compiling, you need to add the relevant paths to the PATH and LD_LIBRARY_PATH. You can execute the following two lines (everytime you reset the machine) or add them to "~/.bashrc" file to automatically append.
```
export PATH=$HOME/sfw/MPICH/bin:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/$HOME/sfw/Boost/Boost_1.61/lib/:/$HOME/sfw/repast_hpc-2.3.0/lib/
```


# Set up and run the example mode
* Download or clone the repo.
* Both "core" and "norms" folders are required for compiling.
* Please edit the "env" file in the "norms" folder to match with your installation paths and versions of RepastHPC and Boost.
* If this is __the first time__, please __change directory to the SchellingModelExample__, use the following command to create relevant folders and compile the model:
```
make all
```
* Run the model with the following command:
```
mpirun -n 1 ./bin/main.exe ./props/config.props ./props/mode.props
```
* The simulated results are in "outputs" folder.
* You can do the three experiments in the paper by changing the value of "experiment=0" in ./props/model.props file (without recompiling). Set "experiment" variable to 1, 2, or 3 to perform the corresponding experiment.


# Other technical notes:
* When modifying the source code, use this command for faster compiling time.
```
make compile
```
* If a class is added (a new cpp file), "makefile" needs to be updated.
* The compiling use header and source files in both core and SchellingModelExample folder, but the all the object files \*.o are in "./SchellingModelExample/objects/" folder and the executable file main.exe is in "./SchellingModelExample/bin/" folder.
