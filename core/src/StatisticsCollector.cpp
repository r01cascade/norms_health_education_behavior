#include "StatisticsCollector.h"

StatisticsCollector::StatisticsCollector(repast::SharedContext<Agent>* pPopulation) {
	mpPopulation = pPopulation;
}

// Have a boost::bad_get exception if getting an existing key using a wrong type
bool StatisticsCollector::exists(std::string key) {
  if (mMap.find(key) == mMap.end())
    return false;
  else return true;
}

void StatisticsCollector::collectAgentStatistics() {
	int vPopulation =0;
	int vMale =0;
	int vFemale =0;
	int vAgeGroup1 =0;
	int vAgeGroup2 =0;
	int vAgeGroup3 =0;
	int vAgeGroup4 =0;
	int v12MonthDrinkers =0;
	int v12MonthDrinkersMale =0;
	int v12MonthDrinkersFemale =0;
	int v12MonthDrinkersAgeGroup1 =0;
	int v12MonthDrinkersAgeGroup2 =0;
	int v12MonthDrinkersAgeGroup3 =0;
	int v12MonthDrinkersAgeGroup4 =0;
	double vQuantMale =0;
	double vQuantFemale =0;
	double vQuantAgeGroup1 =0;
	double vQuantAgeGroup2 =0;
	double vQuantAgeGroup3 =0;
	double vQuantAgeGroup4 =0;
	int vFreqMale =0;
	int vFreqFemale =0;
	int vFreqAgeGroup1 =0;
	int vFreqAgeGroup2 =0;
	int vFreqAgeGroup3 =0;
	int vFreqAgeGroup4 =0;
	int vSumOccasionalHeavyDrinking =0;

	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {

		vPopulation += 1;
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
			v12MonthDrinkers += 1;
			vSumOccasionalHeavyDrinking += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
		}

		if ((*iter)->getSex() == MALE) {
			vMale += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				v12MonthDrinkersMale += 1;
				vQuantMale += (*iter)->getAvgDrinksNDays(30);
				vFreqMale += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		} else if ((*iter)->getSex() == FEMALE) {
			vFemale += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				v12MonthDrinkersFemale += 1;
				vQuantFemale += (*iter)->getAvgDrinksNDays(30);
				vFreqFemale += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		}

		if ((*iter)->getAge() >= 12 && (*iter)->getAge() <= 17) {
			vAgeGroup1 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				v12MonthDrinkersAgeGroup1 += 1;
				vQuantAgeGroup1 += (*iter)->getAvgDrinksNDays(30);
				vFreqAgeGroup1 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		} else if ((*iter)->getAge() >= 18 && (*iter)->getAge() <= 34) {
			vAgeGroup2 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				v12MonthDrinkersAgeGroup2 += 1;
				vQuantAgeGroup2 += (*iter)->getAvgDrinksNDays(30);
				vFreqAgeGroup2 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		} else if ((*iter)->getAge() >= 35 && (*iter)->getAge() <= 64) {
			vAgeGroup3 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				v12MonthDrinkersAgeGroup3 += 1;
				vQuantAgeGroup3 += (*iter)->getAvgDrinksNDays(30);
				vFreqAgeGroup3 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		} else if ((*iter)->getAge() >= 65) {
			vAgeGroup4 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				v12MonthDrinkersAgeGroup4 += 1;
				vQuantAgeGroup4 += (*iter)->getAvgDrinksNDays(30);
				vFreqAgeGroup4 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		}

		iter++;
	}

	set("Population",vPopulation);
	set("Male",vMale);
	set("Female",vFemale);
	set("AgeGroup1",vAgeGroup1);
	set("AgeGroup2",vAgeGroup2);
	set("AgeGroup3",vAgeGroup3);
	set("AgeGroup4",vAgeGroup4);
	set("12MonthDrinkers",v12MonthDrinkers);
	set("12MonthDrinkersMale",v12MonthDrinkersMale);
	set("12MonthDrinkersFemale",v12MonthDrinkersFemale);
	set("12MonthDrinkersAgeGroup1",v12MonthDrinkersAgeGroup1);
	set("12MonthDrinkersAgeGroup2",v12MonthDrinkersAgeGroup2);
	set("12MonthDrinkersAgeGroup3",v12MonthDrinkersAgeGroup3);
	set("12MonthDrinkersAgeGroup4",v12MonthDrinkersAgeGroup4);
	set("QuantMale",vQuantMale);
	set("QuantFemale",vQuantFemale);
	set("QuantAgeGroup1",vQuantAgeGroup1);
	set("QuantAgeGroup2",vQuantAgeGroup2);
	set("QuantAgeGroup3",vQuantAgeGroup3);
	set("QuantAgeGroup4",vQuantAgeGroup4);
	set("FreqMale",vFreqMale);
	set("FreqFemale",vFreqFemale);
	set("FreqAgeGroup1",vFreqAgeGroup1);
	set("FreqAgeGroup2",vFreqAgeGroup2);
	set("FreqAgeGroup3",vFreqAgeGroup3);
	set("FreqAgeGroup4",vFreqAgeGroup4);
	set("SumOccasionalHeavyDrinking",vSumOccasionalHeavyDrinking);

	collectTheoryAgentStatistics();
}
