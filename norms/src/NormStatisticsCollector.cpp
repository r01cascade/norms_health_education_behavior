#include "NormStatisticsCollector.h"

NormStatisticsCollector::NormStatisticsCollector(repast::SharedContext<Agent>* pPopulation) : StatisticsCollector(pPopulation) {}

void NormStatisticsCollector::collectTheoryAgentStatistics() {
	int vPop_G3G4_M =0;
	int vPop_G3G4_F =0;

	int vDrinker_G3G4_M =0;
	int vDrinker_G3G4_F =0;

	double vQuant_G3G4_M =0;
	double vQuant_G3G4_F =0;

	int vFreq_G3G4_M =0;
	int vFreq_G3G4_F =0;

	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		//the targeted groups for Experiment 2 (g3 and g4 out of 9, age 35-44 and 45-54)
		if ((*iter)->getAge() >= 35 && (*iter)->getAge() <= 54) {
			if ((*iter)->getSex() == MALE) {
				vPop_G3G4_M += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinker_G3G4_M += 1;
					vQuant_G3G4_M += (*iter)->getAvgDrinksNDays(30);
					vFreq_G3G4_M += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				}
			} else if ((*iter)->getSex() == FEMALE) {
				vPop_G3G4_F += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinker_G3G4_F += 1;
					vQuant_G3G4_F += (*iter)->getAvgDrinksNDays(30);
					vFreq_G3G4_F += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				}
			}
		}
		iter++;
	}

	set("Pop_G3G4_M",vPop_G3G4_M);
	set("Pop_G3G4_F",vPop_G3G4_F);
	set("Drinker_G3G4_M",vDrinker_G3G4_M);
	set("Drinker_G3G4_F",vDrinker_G3G4_F);
	set("Quant_G3G4_M",vQuant_G3G4_M);
	set("Quant_G3G4_F",vQuant_G3G4_F);
	set("Freq_G3G4_M",vFreq_G3G4_M);
	set("Freq_G3G4_F",vFreq_G3G4_F);
}